const express = require('express');
const { pool } = require("./db");
const exphbs = require('express-handlebars');
const path = require('path');
const methodOverride = require('method-override');
const session = require('express-session'); 
const flash = require('connect-flash');
const passport = require('passport');

//inicializaciones
const app = express();
require('./config/passport');

//Configuracion
app.set('port', process.env.PORT || 3000);
app.use(express.static(__dirname));

app.set('views',path.join('views'));
app.engine('.hbs', exphbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname:'.hbs'
}));
app.set('view engine', '.hbs');


//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'mihsecreto',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Global
app.use((req,res, next)=>{
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

//rutas
app.use(require('./routes/index'));
app.use(require('./routes/usuario'));
app.use(require('./routes/admin'));
app.use(require('./routes/lider'));
app.use(require('./routes/agente'));

//Server 
app.listen(app.get('port'), ()=>{
    console.log('servidor en el puerto', app.get('port'));
});
