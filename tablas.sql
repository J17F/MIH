CREATE TABLE usuarios(
  username VARCHAR(50) NOT NULL PRIMARY KEY,
  contrasena VARCHAR(50) NOT NULL,
  nombre VARCHAR(50) NOT NULL,
  apellido VARCHAR(50) NOT NULL,
  email VARCHAR(50) ,
  administrador BOOLEAN,
  lider BOOLEAN,
  agente BOOLEAN
);
CREATE TABLE campaign(
  id SERIAL NOT NULL PRIMARY KEY,
  nombre VARCHAR(100) NOT NULL,
  proposito VARCHAR(250) NOT NULL,
  monto INT NOT NULL,
  total INT
);
CREATE TABLE donacion( 
  orden SERIAL NOT NULL PRIMARY KEY,
  monto INT NOT NULL,
  descripcion VARCHAR(50) NOT NULL,
  n_usuario VARCHAR(50) NOT NULL REFERENCES usuarios(username),
  id_campaign INT NOT NULL REFERENCES campaign(id),
  tipo VARCHAR(50)
);
CREATE TABLE venta(
  n_orden INT REFERENCES donacion(orden),
  servicio VARCHAR(50) NOT NULL,
  venta BOOLEAN NOT NULL
);
CREATE TABLE producto(
  codigo SERIAL NOT NULL PRIMARY KEY,
  id_campaign INT NOT NULL REFERENCES campaign(id),
  nombre VARCHAR(100) NOT NULL,
  descripcion VARCHAR(250) NOT NULL
);
CREATE TABLE incluye(
  n_orden INT REFERENCES donacion(orden),
  cod_producto INT REFERENCES producto(codigo)
);
CREATE TABLE pertenece(
  n_usuario VARCHAR(50) NOT NULL REFERENCES usuarios(username),
  id_campaign INT NOT NULL REFERENCES campaign(id),
  ref VARCHAR(20),
  contador INT
);

INSERT INTO usuarios(username, contrasena, nombre, apellido, administrador, lider, agente)
VALUES ('Todos', 'nosotros', 'Todos', 'Nosotros', 'TRUE', 'TRUE', 'TRUE');

INSERT INTO usuarios(username, contrasena, nombre, apellido, administrador)
VALUES ('Admin', 'admin123', 'Todos', 'Nosotros', 'TRUE');

INSERT INTO usuarios(username, contrasena, nombre, apellido, lider)
VALUES ('Lider', 'lider123', 'Todos', 'Nosotros', 'TRUE');

INSERT INTO usuarios(username, contrasena, nombre, apellido, agente)
VALUES ('Agente', 'agente123', 'Todos', 'Nosotros', 'TRUE');


INSERT INTO campaign(nombre, proposito, monto)
VALUES ('Lucha contra el cancer', 'Ayuda a los niños a luchar contra el cancer', 500000);

INSERT INTO campaign(nombre, proposito, monto)
VALUES ('Necesito medicinas', 'Tengo diabetes y necesito comprar mis medicinas', 100000);

INSERT INTO campaign(nombre, proposito, monto)
VALUES ('Ayuda a Maria', 'Maria necesita de tu ayuda para realizar una operacion', 60000);

INSERT INTO campaign(nombre, proposito, monto)
VALUES ('Ayudanos a pasar', 'Necesitamos salir bien en todas las materias', 1000000);


INSERT INTO donacion(monto, descripcion, n_usuario, id_campaign, tipo)
VALUES (100, 'Para ayudar contra el cancer', 'Agente', 1, 'Voluntaria');

INSERT INTO donacion(monto, descripcion, n_usuario, id_campaign, tipo)
VALUES (1000, 'Donacion de ayuda', 'Todos', 3, 'Voluntaria');

INSERT INTO donacion(monto, descripcion, n_usuario, id_campaign, tipo)
VALUES (200, 'Debes comprar tus medicinas', 'Lider', 2, 'Voluntaria');

INSERT INTO donacion(monto, descripcion, n_usuario, id_campaign, tipo)
VALUES (20, 'Ustedes pasaron', 'Todos', 4, 'Venta');


INSERT INTO venta(n_orden,servicio,venta)
VALUES (4, 'Compra de camisa', 'TRUE');


INSERT INTO producto(id_campaign,nombre,descripcion)
VALUES (4, 'Franelas de colores', 'Ayuda a esta campaña con la compra de franelas de colores');


INSERT INTO incluye(n_orden,cod_producto)
VALUES (4, 1);


INSERT INTO pertenece(n_usuario,id_campaign)
VALUES ('Lider', 4);