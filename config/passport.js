const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const { pool } = require("../db");


passport.use(
    new LocalStrategy(
      {
        usernameField: "username",
      },
      async (username, password, done) => {
        // Match Email's User
        const user = await pool.query('SELECT * FROM usuarios WHERE username = $1', [username]);
        if (user.rows[0]) {
          // Match Password's User
          if (password === user.rows[0].contrasena) {
            return done(null, user.rows[0].username);
          } else {
            return done(null, false, { message: "Incorrect Password." });
          }
          
        }else {
          return done(null, false, { message: "Not User found." });
        }
      }
    )
  );

  passport.serializeUser((user, done) => {
    done(null, user);
  });
  
  passport.deserializeUser(async (username, done) => {
    const user = await pool.query('SELECT * FROM usuarios WHERE username = $1', [username]);
    done(null,user.rows[0]);
  });