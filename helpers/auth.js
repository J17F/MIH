const helpers = {};

helpers.isAuthenticated = (req, res, next) =>{
    if (req.isAuthenticated()) {
        return next();
    }
    req.flash("error_msg", "No autorizado.");
    res.redirect("/");
}

helpers.isAdmin = (req, res, next) => {
    if(req.user.administrador){
        return next();
    }
    req.flash("error_msg", "No autorizado.");
    res.redirect("/");
}

helpers.isAgent = (req, res, next) => {
    if(req.user.agente){
        return next();
    }
    req.flash("error_msg", "No autorizado.");
    res.redirect("/");
}

helpers.isLider = (req, res, next) => {
    if(req.user.lider){
        return next();
    }
    req.flash("error_msg", "No autorizado.");
    res.redirect("/");
}
module.exports = helpers;