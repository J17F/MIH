const express = require('express');
const router = express.Router();
const { pool } = require("../db");
const { isAuthenticated, isAdmin } = require('../helpers/auth');

router.get('/admin',isAuthenticated, isAdmin, (req,res)=>{
    res.render('admin',{ title: 'Admin - Make It Happen', layout: 'usuario' })
});

router.get('/registrar',isAuthenticated, isAdmin, (req,res)=>{
    res.render('registrar',{ title: 'Admin - Make It Happen', layout: 'usuario' })
});

router.post('/registrar', (req,res)=>{
    let { nombre, apellido, username, email, password, password2, admin, lider, agente } = req.body;

    let errors = [];

    if(!nombre || !apellido || !username || !password || !password){
        errors.push({message:"Por favor ingrese todos los campos"});
    }

    if (password != password2){
        errors.push({message:"Las contraseñas no son iguales"});
    }
    
    if(!admin && !lider && !agente){
        errors.push({message:"Debe seleccionar al menos 1 tipo de usuario"})
    }

    if (errors.length > 0){
        res.render('registrar',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
    }else{
        pool.query(
            'SELECT * FROM usuarios WHERE username = $1',
            [username],
            (err,results)=>{
                if(err){
                    throw err;
                }
                console.log(results.rows);
                if(results.rows.length > 0){
                    errors.push({message:"Nombre de usuario ya tomado"});
                    res.render('registrar',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
                }else{
                    if(admin || lider || agente){
                        pool.query(
                            'INSERT INTO usuarios (username, contrasena, nombre, apellido, email, administrador, lider, agente) VALUES($1,$2,$3,$4,$5,$6,$7,$8)',
                            [username,password,nombre,apellido,email,admin,lider,agente],
                            (err,results)=>{
                                if (err){
                                    throw err;
                                }
                                console.log(results.rows);
                            }
                        )
                    }
                    res.render('registrar',{ title: 'Admin - Make It Happen', layout: 'usuario', success_msg:'Usuario registrado exitosamente' })
                }
            }
        ) 
    }
});

router.get('/registrar-campana', (req,res)=>{
    res.render('registrar-campana',{ title: 'Admin - Make It Happen', layout: 'usuario' })
});

router.post('/registrar-campana', (req,res)=>{
    let { nombre, descripcion, monto } = req.body;

    let errors = [];

    if(!nombre || !descripcion || !monto){
        errors.push({message:"Por favor ingrese todos los campos"});
    }

    if (errors.length > 0){
        res.render('registrar-campana',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
    }else{
        pool.query(
            'INSERT INTO campaign (nombre, proposito, monto) VALUES($1,$2,$3)',
            [nombre,descripcion,monto],
            (err,results)=>{
                if (err){
                    throw err;
                }
                console.log(results.rows);
            }
        )
        res.render('registrar-campana',{ title: 'Admin - Make It Happen', layout: 'usuario', success_msg:'Campaña registrada exitosamente' })
    }
});

router.get('/asigna-lider',(req,res)=>{
    res.render('asigna-lider',{ title: 'Admin - Make It Happen', layout: 'usuario' })
});

router.post('/asigna-lider',(req,res)=>{
    let { username, id } = req.body;

    let errors = [];

    if(!username || !id){
        errors.push({message:"Por favor ingrese todos los campos"});
    }

    if (errors.length > 0){
        res.render('asigna-lider',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
    }else{
        pool.query(
            'SELECT * FROM usuarios WHERE username = $1',
            [username],
            (err,results)=>{
                if(err){
                    throw err;
                }
                console.log(results.rows);
                if(results.rows.length == 0){
                    errors.push({message:"usuario no existe"});
                    res.render('asigna-lider',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
                }else{
                    pool.query(
                        'SELECT * FROM campaign WHERE id = $1',
                        [id],
                        (err,results)=>{
                            if (err){
                                throw err;
                            }
                            console.log(results.rows);
                            if(results.rows.length == 0){
                                errors.push({message:"campaña no existe"});
                                res.render('asigna-lider',{ title: 'Admin - Make It Happen', layout: 'usuario' , errors });
                            }else{
                                pool.query(
                                    'INSERT INTO pertenece (n_usuario,id_campaign) VALUES ($1,$2)',
                                    [username,id],
                                    (err,results)=>{
                                        if (err){
                                            throw err;
                                        }
                                        console.log(results.rows);
                                    }
                                )
                                res.render('asigna-lider',{ title: 'Admin - Make It Happen', layout: 'usuario', success_msg:'Lider asignado correctamente' })
                            }
                        }
                    )
                   
                }
            }
        )
    }
});

module.exports = router;