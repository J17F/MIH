const express = require('express');
const router = express.Router();
const { pool } = require("../db");
const { isAuthenticated, isLider } = require('../helpers/auth');

router.get('/lider', isAuthenticated, isLider, async (req,res)=>{
    
    res.render('lider',{ title: 'Lider - Make It Happen', layout: 'usuario' })
});

router.get('/modificar-campana',(req,res)=>{
    res.render('modificar-campana',{ title: 'Lider - Make It Happen', layout: 'usuario' })
});

router.get('/asigna-lider',(req,res)=>{
    res.render('asigna-lider',{ title: 'Lider - Make It Happen', layout: 'usuario' })
});

router.get('/agregar-producto',(req,res)=>{
    res.render('agregar-producto',{ title: 'Admin - Make It Happen', layout: 'usuario' })
});

module.exports = router;