const express = require('express');
const router = express.Router();
const { pool } = require("../db");
const { isAuthenticated, isAgent } = require('../helpers/auth');

router.get('/agent',isAuthenticated, isAgent, (req,res)=>{
    res.render('agente',{ title: 'Agente - Make It Happen', layout: 'usuario' })
});

module.exports = router;