const express = require('express');
const router = express.Router();
const { pool } = require("../db");
const passport = require('passport');

router.get('/admin-login', (req,res)=>{
    nav = false;
    res.render('admin-login',{nav})
});

router.post('/admin-login', passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/admin-login',
    failureFlash: true
}));

router.get('/agente-login', (req,res)=>{
    nav = false;
    res.render('agente-login',{nav})
});

router.post('/agente-login', passport.authenticate('local', {
    successRedirect: '/agent',
    failureRedirect: '/agente-login',
    failureFlash: true
}));

router.get('/lider-login', (req,res)=>{
    nav = false;
    res.render('lider-login',{nav})
});

router.post('/lider-login', passport.authenticate('local', {
    successRedirect: '/lider',
    failureRedirect: '/lider-login',
    failureFlash: true
}));

router.get('/logout', (req,res)=>{
    req.logout();
    res.redirect("/");
});

module.exports = router;
