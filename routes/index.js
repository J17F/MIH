const express = require('express');
const router = express.Router();
const { pool } = require("../db");



router.get('/', (req,res)=>{
    res.render('index',{ title: 'Make It Happen', nav: true});
});

router.get('/donacion', (req,res)=>{
    res.render('donacion',{ title: 'Donacion - Make It Happen', nav: true});
});

module.exports = router;
