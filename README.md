# Make.It.Happen

Crowdfunding Web App

Esta es una Aplicación web de donaciones para la recaudación de fondos

Instrucciones para la prueba de la aplicacion.
 
1-Clonar el repositorio.
2-Instalar dependencias.
3-Dentro de el archivo db.js, cambie los parámetros encontrados dentro de ese archivo dependiendo de sus datos de postgres.
4- Crear la base de datos dentro de psql usando el comando -> CREATE DATABASE mih;
5- Dentro de psql, usar el comando para conectarse a la base de datos mih -> \c mih
6- Al conectarse a la base de datos, usar el comando \i + la ubicacion del documento tablas.sql que se encuentra dentro de esta carpeta. Por ejemplo si esta en Windows deberia ser un comando similar-> \i 'C:/Users/Joseph/Desktop/make.it.happen/tablas.sql'
7-Utilizar el comando -> npm start <- para iniciar el funcionamiento del proyecto.
8-Abrir el navegador y dirigirse a la Aplicación.



















Equipo de desarrollo
  María López V-27.182.207
  José Gómez V-27.547.101
  Luisa Hernández V-27.525.359
  Lusandre Marcano V-28.349.644